package com.qa;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class SimpleTest {
    public static void main(String[] args) throws InterruptedException {
        // ***** write code
        System.out.println("Running SimpleTest!");
        // ***** look for kind of machine
        String os = System.getProperty("os.name").toLowerCase();
        // ***** set setProperty value to select firefox or chrome browser
        if(os.contains("mac")){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver");
        }else{
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\geckodriver.exe");
        }
        // ***** Open Driver
        //WebDriver driver = new FirefoxDriver();
        WebDriver driver = new ChromeDriver();
        // ***** Go to page to test
        driver.get("https://www.guatecompras.gt");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
        // ***** Close 'wellcome modal'
        ((ChromeDriver) driver).findElementByXPath("/html/body/div[4]/div[1]/button").click();

        // ***** Click on Login Button
        WebElement loginButton = ((ChromeDriver) driver).findElementById("menu_gc_login");
        loginButton.click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MICROSECONDS);

        // ***** Click on User Input
        WebElement passInput = ((ChromeDriver) driver).findElementById("MasterGC_ContentBlockHolder_myLogin_txtPassword");
        passInput.click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
        // ***** get pass-icon by xpath
        WebElement passIcon=driver.findElement(By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[2]"));
        // ***** Verify pass-icon appear
        Assert.assertEquals(passIcon.getAttribute("class"), "pass-icon iconoVisible", "el icono del pass NO es visible ");

        // ***** Click Password Input
        WebElement userInput = ((ChromeDriver) driver).findElementById("MasterGC_ContentBlockHolder_myLogin_txtUser");
        userInput.click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
        // ***** get user-icon by xpath
        WebElement userIcon= driver.findElement(By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[1]"));
        // ***** Verify user-icon appear
        Assert.assertEquals(userIcon.getAttribute("class") ,"user-icon iconoVisible", "el icono del user NO es visible");

        driver.quit();
        System.out.println("fin del test");
    }
}
