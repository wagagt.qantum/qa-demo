package com.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestLoginIcons {

	WebDriver driver;
	Integer testNumber = 1;
	final String baseUrl = "http://www.guatecompras.gt";

	@BeforeClass
	public void beforeClass() {
		System.out.println("- - - - - - - - - - - - INICIO de la Clase");
	}

	@BeforeMethod
	public void beforeMethod(ITestResult result) {
		System.out.println(result.getMethod().getDescription());
		System.out.println("************************ Inicio Test " + result.getMethod().getDescription() + " - NO:"
				+ testNumber.toString());
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("mac")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
		} else {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		}
		driver = new ChromeDriver();
		driver.get("https://www.guatecompras.gt");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
	}

	// TestRail id = 1: Validar que aparezcan los iconos de user/pass en la pagina
	// de login.
	@Test(description = "testLoginIconsAppear", priority = 1, enabled = true)
	public void testLoginIconsAppear() {
		((ChromeDriver) driver).findElementByXPath("/html/body/div[4]/div[1]/button").click();
		WebElement loginButton = ((ChromeDriver) driver).findElementById("menu_gc_login");
		loginButton.click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MICROSECONDS);
		WebElement passInput = ((ChromeDriver) driver)
				.findElementById("MasterGC_ContentBlockHolder_myLogin_txtPassword");
		passInput.click();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
		WebElement passIcon = driver
				.findElement(By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[2]"));
		WebElement userInput = ((ChromeDriver) driver).findElementById("MasterGC_ContentBlockHolder_myLogin_txtUser");
		userInput.click();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.MICROSECONDS);
		WebElement userIcon = driver
				.findElement(By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[1]"));
		Assert.assertEquals(userIcon.getAttribute("class"), "user-icon iconoVisible",
				"el icono del user NO es visible");
	}

	// TestRail id = 2: Validar que se muestre el modal en el home.
	@Test(description = "testHomeModal", priority = 2, enabled = true)
	public void testHomeModal() {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MICROSECONDS);
		WebElement modalWindow = driver.findElement(By.xpath("/html/body/div[4]"));
		Assert.assertEquals(modalWindow.isDisplayed(), true);
	}

	// TestRail id =3: Locators
	@Test(description = "testLocators", priority = 3, enabled = true)
	public void testLocators() {
		SoftAssert softAssert = new SoftAssert();
		// get footer by 'id' / el mejor y mas facil de usar
		WebElement footerElement = driver.findElement(By.id("pagefooter"));

		// get by 'name' / bueno y facil de usar ( name podria repetirse / id no deberia
		// )
		WebElement metaContent = driver.findElement(By.name("keywords"));

		// get by 'link_text'
		WebElement terminosYCondicionesLinkText = driver.findElement(By.linkText("Términos y Condiciones de Uso"));
		WebElement terminosYCondicionesPartialLinkText = driver.findElement(By.partialLinkText("nos y Condici"));

		// get by 'link_text' REPETIDO
		WebElement buscarLinkText = driver.findElement(By.linkText("Buscar")); // toma el primero
		List<WebElement> buscarLinkTexts = driver.findElements(By.linkText("Buscar")); // devuelve una lista -> evaluate
																						// :
																						// buscarLinkTexts.get(0).getAttribute("id")

		// get by 'css-locator' or 'Xpath' https://devhints.io/xpath
		WebElement celdaRoja = driver.findElement(By.xpath("//table//tr[5]//td[5]/a"));

		// asserts
		softAssert.assertEquals(false, false, "valores fijos -> true <> false");

		System.out.println("- assert footerElement");
		softAssert.assertNotEquals(footerElement.isDisplayed(), false, "footerElement.isDisplayed"); // logica inversa

		System.out.println("- assert metaContent");
		softAssert.assertTrue(metaContent.getAttribute("content").contains("Guatecompras"));
		softAssert.assertTrue(metaContent.getAttribute("content").contains(
				"Guatecompras, Guatemala, Compras, eProcurement, eGovernment, Transparencia, Todo a la vista de todos, Mercado electrónico, Gobierno de Guatemala, Licitaciones, Cotizaciones, Concursos, Ministerio de Finanzas Públicas"));

		System.out.println("- assert terminos y condiciones");
		softAssert.assertTrue(
				terminosYCondicionesLinkText.getAttribute("href").equals(baseUrl + "/info/TerminosDeUso.aspx"));
		softAssert.assertTrue(
				terminosYCondicionesPartialLinkText.getAttribute("href").equals(baseUrl + "/info/TerminosDeUso.aspx"));

		System.out.println("- assert celda Roja - class LinkFijoRojo");
		softAssert.assertTrue(celdaRoja.getAttribute("class").equals("LinkFijoRojo"));

		softAssert.assertAll();
	}

	// TestRail id =4: javascript
	@Test(description = "evaluate javascript", priority = 4, enabled = true)	
	public void testWithJavascript() {
		((ChromeDriver) driver).findElementByXPath("/html/body/div[4]/div[1]/button").click();
		String titleFromJS = ((JavascriptExecutor)driver).executeScript("return document.title;").toString();
		System.out.println("********** Page Title = " + titleFromJS);
		Assert.assertEquals(titleFromJS, "Guatecompras - Sistema de Contrataciones y Adquisiciones del Estado de Guatemala");
		// example 2  ( validate tooltip over Boletines GC
		WebElement boletinesImage= driver.findElement(By.xpath("//img[@src='images/panal_BoletinDiario1.png']"));
//		((JavascriptExecutor)driver).executeScript("$('.imagenes_fondo:has(img[alt=\"Boletines\"])').mouseover();").toString();
		
		((JavascriptExecutor)driver).executeScript("var mouseEvent = document.createEvent('MouseEvents');mouseEvent.initEvent('mouseover', true, true); arguments[0].dispatchEvent(mouseEvent);", boletinesImage);
		WebElement tooltipLinksBoletin = driver.findElement(By.id("LinksBoletin"));
		Assert.assertEquals(tooltipLinksBoletin.isDisplayed(), true);
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
		System.out.println("************************ Fin Test No. " + testNumber.toString());
		testNumber++;
	}

	@AfterClass
	public void endClass() {
		System.out.println("- - - - - - - - - -  - - FIN de la Clase");
	}
}