package com.utils;

public class HomePage extends PageObject {
	
	private final static String pageTitle = "Guatecompras - Sistema de Contrataciones y Adquisiciones del Estado de Guatemala";
	private final static String pageUrl = "/";
	
	public WebElement slideShow = ("//*[@id=\"MasterGC_ContentBlockHolder_slideShow\"]");
	
	
	public HomePage(String pageTitle, String pageUrl) {
		super(pageTitle, pageUrl);
		// TODO Auto-generated constructor stub
	}
	

}
